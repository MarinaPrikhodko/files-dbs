package com.company;

import java.io.IOException;

public class Client {

    public static void main(String[] args)throws IOException, ClassNotFoundException {
        lifecycleFileData();
    }

    public static void lifecycleFileData() throws IOException, ClassNotFoundException {
        NoteDao executor = new NoteDao();
        executor.openStream();
//        executor.write(new Note(222, "Message from D"));

        Note [] notes=new Note [5];
        notes [0]= new Note(24, "Hello");
        notes [1]= new Note(25, "Hi");
        notes [2]= new Note(26, "Good Luck");
        notes [3]= new Note(27, "Have a nice day");
        notes [4]= new Note(28, "Bye Bye");

//        List<Note> notes = executor.read();
        for (Note note: notes) {
            System.out.println("id: " + note.getId() + " " + "message: " + note.getMessage());
        }

        executor.closeStream();
    }
}


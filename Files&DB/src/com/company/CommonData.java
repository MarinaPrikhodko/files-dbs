package com.company;

public abstract class CommonData {
    int id;

    CommonData() {
    }

    public CommonData(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}

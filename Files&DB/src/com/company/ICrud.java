package com.company;

public interface ICrud {
    void update (CommonData data);
    void delete (int id);
    void clearAll ();
}

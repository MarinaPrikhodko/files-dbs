package com.company;

import java.util.List;

public interface IDataBase extends ICrud{
    void connect();
    void disconnect();
    void insert(User user);
    List<User> selectAll();

}

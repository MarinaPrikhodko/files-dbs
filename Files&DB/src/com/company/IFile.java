package com.company;

import java.io.IOException;
import java.util.List;

public interface IFile extends ICrud{
    void write(Note note) throws IOException;
    List<Note> read () throws IOException, ClassNotFoundException;
    void openStream ();
    void closeStream ();

}

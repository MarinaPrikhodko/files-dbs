package com.company;

import java.io.Serializable;

public class Note extends CommonData implements Serializable {

    String message="Hello World";

    public Note(int id, String message) {
        super(id);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}

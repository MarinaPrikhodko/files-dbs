package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class NoteDao implements IFile{
    @Override
    public void write(Note note) throws IOException {
        OutputStream outputStream = new FileOutputStream("text.txt", false);
        ObjectOutputStream bos = new ObjectOutputStream(outputStream);
        bos.writeObject(note);
    }

    @Override
    public List<Note> read() throws IOException, ClassNotFoundException {
        List<Note> notes = new ArrayList<>();
        InputStream is = new FileInputStream("text.txt");
        ObjectInputStream ois = new ObjectInputStream(is);
        while (ois.readObject() != null) {
            Object note = ois.readObject();
            notes.add((Note) note);
        }
        return notes;
    }

    @Override
    public void openStream() {
        System.out.println("Stream was opened");
    }

    @Override
    public void closeStream() {
        System.out.println("Stream was closed");
    }

    @Override
    public void update(CommonData data) { // Note

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void clearAll() {

    }
}

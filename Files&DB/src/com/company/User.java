package com.company;

public class User extends CommonData {

    private String name;

    public User(int id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}



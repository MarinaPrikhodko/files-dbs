package com.company;

import java.util.List;

public class UserDao implements IDataBase {
    @Override
    public void connect() {
        System.out.println();
    }

    @Override
    public void disconnect() {

    }

    @Override
    public void insert(User user) {

    }

    @Override
    public List<User> selectAll() {
        return null;
    }

    @Override
    public void update(CommonData data) { // User

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void clearAll() {

    }
}
